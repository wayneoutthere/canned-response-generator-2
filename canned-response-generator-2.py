#!/usr/bin/env python3

import re
import json
import xml.etree.ElementTree as ET
import os

tree = ET.parse("data/fake_dept/action_dir/wayson.xml")
root = tree.getroot()

print("Tree is ", tree)
print("Root is ", root)

cwd = os.getcwd()
print("current working directory is: ", cwd)

DEPT_PATH = os.path.join(cwd) + '/data/fake_dept/'
# print("DEPT_PATH at L14 is ", DEPT_PATH)

def list_dir_items():
    dir = os.listdir(DEPT_PATH)
    index_list = []
    filename_list = []
    # 
    for index, filename in enumerate(dir):
        index_list.append(index +1)
        filename_list.append(filename)

    for x, y in zip(index_list, filename_list):
        print("|", x, "| ~~~~~~" , y)

list_dir_items()



def change_dir():
    dir_choice = int(input(" > " -1)
    


## Made 'JSON_TEMPLATES' so that we can work with the template options in the 
## JSON file easily
JSON_TEMPLATES = data['templates']

## Made 'JSON_CHOICES' as a fixed variable which will be called when script 
## digs into the 'choices' in the JSON file
JSON_CHOICES = data['choices']

## templates_list takes the 'template' keys in the JSON file and turns them into
## a variable for purpose of creating human readable list with numbers for user input.
templates_list= list(JSON_TEMPLATES.keys())

print("Please enter a number (only) of the TEMPLATE you wish to use from the following list: ")

for i, j in enumerate(templates_list):
    print(i + 1, j)


## --------- SECTION TO DELETE ---------------- ###
## listToNum is a variable that takes a list and can be inserted into the opt_num_maker
#listToNum = templates_list

## -------- Option-Number-Maker Function  -----------------
## opt_num_maker function iterates through the list (listToNum) and prints out 
## each item and put a human readable (as opposed to python list index number) 
## number in front of it.

def opt_num_maker(listToNum):
    listIndex = 0
    optionNum = 1
    for listIndex in range(len(listToNum)):
        print(optionNum, listToNum[listIndex])
        optionNum += 1

## Running the opt_num_maker prints out the list for user
#opt_num_maker(listToNum)
##------------------------------------##



## --------- SECTION TO DELETE ---------------- ###
## User selects the template number (number only)
#user_input = input(" > ")
#####listChoice = int(user_input) -1
## NEXT LINE JUST FOR DEBUGGING - TO BE DELETED
# print("datatype of ListChoice on line 56 is: " , type(listChoice))
##------------------------------------##

## `make_listChoice` converts the users input into an integer so it can 
## feed back into list index number in the function 'return_list_choice'. 
## It subtracts 1 so that it doesn't go out of range when user chooses 
## the last human readable number in the list.

def make_listChoice_int():
    user_input = input(" > ")
    listChoiceInt = int(user_input) -1
    return (listChoiceInt)


#Run `listChoiceInt`
listChoiceInt = make_listChoice_int() ## was listChoice (not ListChoiceInt) before.. changed

# make_listChoice_int()
print("listChoice at line 59 is: " , listChoiceInt)

## ----------------- IMPORTANT POINT ------------------------------##
## At this point template selection is made and stuff should occur and 
## loop inside the 'CHOICES' section 

## Did this step so that the returning of the choice can be fed into the' 
## return_list_choice' function. Maybe a better way?

def return_list_choice(listChoiceInt):
    ret_choice_int = templates_list[listChoiceInt]
    return(ret_choice_int)
    
## This 'template_key_finder'is to pull the human-readable string key name 
## (not the int defined in listChoice). Apparently i needed it as one 
## variable in order to use in the string formatting of the upcoming line
## as the function call would only take 'function(call)' format not 
## 'function(int(call))'...

template_key_finder = return_list_choice(listChoiceInt)
## This next line can be uncommented for debugging and seeing data types of line above
##  print("value of key_finder" , template_key_finder, type(template_key_finder))

template_value_finder = JSON_TEMPLATES['{}'.format(template_key_finder)]

## If successful, next line  prints out both the KEY and the VALUE from 
## the associated JSON when uncommented.  For debugging line above.
## print("The TEMPLATE KEY is: **" , template_key_finder , "**  and the TEMPLATE VALUE is: **",  template_value_finder + "**")

## This is Brian Douglass' cool function that digs into the templates in JSON file
## to pull all the curly brackets out and their contents

def extract_choices(template_value_finder):
    '''
    Takes a string and returns a list of all the choices (the values in curly brackets {})
    '''

    choices = re.findall(r'{[^}]+}', template_value_finder)
    choiceList1 = [choice.replace('{', '').replace('}', '') for choice in choices]
    return (choiceList1)  

## Here we are converting the output of 'extract_choices' into a variable
listToNum = extract_choices(template_value_finder)

## Here we run the function 'opt_num_maker' which prints out the list of 'choices'
## and also the 'adjusted' index numbers 
opt_num_maker(listToNum)

print("Please select the option you'd like from the list")

# user_input = input(" >")


## Next line just a data type check to be deleted or used for debugging
## print("Datatype of choice_in is: " , choice_in, type(choice_in))

## This next one is the same as 'listChoice' above in functionality except
## it takes a new variable as argument (the input above).  I feel like maybe this 
## listChoice thing should have been written as a function since i'm using it
## over and over again...
listChoiceInt
#listChoice = int(user_input) -1

## Next one goes into JSON file and pulls the 'key' for Choices.  Since the values
## are ordered lists, we can't seem to use the function above to generate the 
## Human readable list as we could with 'template_value_finder' above which 
## formats a string into the argument.  The 'list' will only take an INT datatype...

## Next one converts the act of running the list-creating function above into a
## new variable, plugging in the new INT into the 'choices_list_printer' below
choices_key_finder = return_list_choice(listChoiceInt)

## Next one makes a new variable which is easier to remember (maybe) than just using 
## 'listChoice'.  Point here is to make it an INT and easier to remember so that
## it can call the list's index position
choices_index_finder = listChoiceInt

## NEXT THREE LINES JUST TO DELETE OR USE FOR DEBUGGING
# print("value of choices_key_finder" , choices_key_finder, type(choices_key_finder))
# print("Value of choices_index_finder: " , choices_index_finder, type(choices_index_finder))
# print("Trying to print out choices list hidden behind" , choices_key_finder , 'next here.')

## 'choices_list_printer' pulls the INT from 'choices_key_finder' above and
## uses that as the index position in the JSON dict in choices.  'JSON_CHOICES'
## is defined way at the top of this script with 'JSON_TEMPLATES' near line 10
choices_list_printer = JSON_CHOICES[choices_key_finder]

## Updating the listToNum variable from earlier so that the subsequent
## opt_num_maker will run with current inputs
listToNum = choices_list_printer

## NEXT LINE JUST PRINTS OUT LIST DATA AS IS IF NEEDED (DEBUGGING)
# print("List contains all these things: ", choices_list_printer)

print("Here are your list of choices to insert into the template: ")
opt_num_maker(listToNum)


print("Select an item from the list: ")

## Prompts user to drill down further in JSON "choices" into the DICT.
## Probably this is where recursion should be used...
choice_in_2 = input(" >")

## NEXT LINE FOR TESTING DEBUGGING AND DELETING
# print("Datatype of choice_in_2 is: " , choice_in_2, type(choice_in))

## Updating 'listChoice' variable with new user input and converting to INT
## this is third occurence of updating 'listChoice' so function seems correct here
listChoice = int(choice_in_2) -1

## Here I need to format the list string using the .join method, I believe
## so that I can make the items in the list appear vertically and with their
## human readable index number + 1 on the left like the other lists above

## 'choices_list_finder' is set as variable so we can present list of data
## in the JSON dict LIST in CHOICES
choices_list_finder = return_list_choice(listChoice)

## next one just cements new variable as an INT and more memorable for the purpose
## since 'listChoice' is used elsewhere for other things and we need INT index position
choices_list_index_finder = listChoice

## NEXT LINES FOR TESTING DEBUGGING AND DELETING
# print("value of choices_list_finder" , choices_list_finder, type(choices_list_finder))
# print("Value of choices_list_index_finder: " , choices_list_index_finder, type(choices_list_index_finder))

choices_list_selection = choices_list_printer[choices_list_index_finder]
print("You have selected: ", choices_list_selection)

print("here is your completed TEMPLATE: ")



# choice

def main():
    pass  # TODO needs implemented


if __name__ == '__main__':
    main()